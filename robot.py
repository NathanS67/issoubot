#!/usr/bin/python3
import http.server
import socketserver
import asyncio
import websockets

from Raspi_MotorHAT import Raspi_MotorHAT, Raspi_DCMotor
from Raspi_PWM_Servo_Driver import PWM
import sys
import json
import os
import picamera
import threading
from camera_robot import *



mh = Raspi_MotorHAT(addr=0x6f)
pwm = PWM(0x6F)
pwm.setPWMFreq(60)

moteur1 = mh.getMotor(1)
moteur2 = mh.getMotor(2)

moteur1.setSpeed(127)
moteur2.setSpeed(127)

pwm.setPWM(0, 0, 375)
pwm.setPWM(1, 0, 400)

#TODO : Sync the speed when a user is connected. : GET INIT -> return config 

class MyHttpRequestHandler(http.server.SimpleHTTPRequestHandler):

    

    def do_GET(self):
        return http.server.SimpleHTTPRequestHandler.do_GET(self)

    def do_POST(self):
        post_body = json.loads(self.rfile.read(
            int(self.headers.get('Content-Length'))))
        if("sens" in post_body):
            sens = post_body["sens"]
            if sens == 0:
                moteur1.run(Raspi_MotorHAT.FORWARD)
                moteur2.run(Raspi_MotorHAT.FORWARD)
            elif sens == 1:
                moteur1.run(Raspi_MotorHAT.FORWARD)
                moteur2.run(Raspi_MotorHAT.BACKWARD)
            elif sens == 2:
                moteur1.run(Raspi_MotorHAT.BACKWARD)
                moteur2.run(Raspi_MotorHAT.BACKWARD)
            elif sens == 3:
                moteur1.run(Raspi_MotorHAT.BACKWARD)
                moteur2.run(Raspi_MotorHAT.FORWARD)
            else:
                moteur1.run(Raspi_MotorHAT.RELEASE)
                moteur2.run(Raspi_MotorHAT.RELEASE)
        if("pan" in post_body):
            pan = post_body["pan"]
            if 150 <= pan <= 600:
                pwm.setPWM(0, 0, pan)
        if("tilt" in post_body):
            tilt = post_body["tilt"]
            if 200 <= tilt <= 600:
                pwm.setPWM(1, 0, tilt)
        if("vitesse" in post_body):
            moteur1.setSpeed(post_body["vitesse"])
            moteur2.setSpeed(post_body["vitesse"])
        if("camera" in post_body):
            camera(post_body["camera"])
        if("pi" in post_body and post_body["pi"] == 0):
            pwm.setPWM(0, 0, 375)
            pwm.setPWM(1, 0, 400)
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(b"")
        

PORTCLIENT = 8080
PORTCAMERA = 8123

def camera_robot():
    with picamera.PiCamera(resolution='640x480', framerate=24) as camera:
        output = StreamingOutput()
        camera.start_recording(output, format='mjpeg')
        try:
            address = ('', PORTCAMERA)
            server = StreamingServer(address, StreamingHandler, output)
            server.serve_forever()
        finally:
            camera.stop_recording()

def client_robot():
    Handler = MyHttpRequestHandler
    with socketserver.TCPServer(("", PORTCLIENT), Handler) as httpd:
        httpd.serve_forever()

connected = set()

async def server(websocket,motorSpeed):
    #register
    connected.add(websocket)
    try:
        async for message in websocket:
            for conn in connected:
                    if conn != websocket:
                        await conn.send({message})
                
        
    finally:
        connected.remove(websocket)

a = threading.Thread(target=camera_robot)
b = threading.Thread(target=client_robot)
a.start()
b.start()
start_server = websockets.serve(server, "localhost", 8081)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
