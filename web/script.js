let touche = "";
let touche1 = "";
let sens = 4;
let vitesse = 127;
let pan = 375;
let tilt = 400;
const MAX_VITESSE = 255;

/** Socket IO
 *  Author: Bash62
 *  Sync speed for all users connected
 */

// Create WebSocket connection.
const socket = new WebSocket('ws://localhost:8081');

// Connection opened
socket.addEventListener('open', function (event) {
    
     document.getElementById("compteur").innerHTML = parseInt((vitesse/MAX_VITESSE)*100) + "%";
     document.getElementById("vitesse").value = vitesse;
     

});



// Listen for messages
socket.addEventListener('message', function (event) {

    compteurDisplayChange(event.data)

});

const changeSpeed = () => {
    let json_config = {
        vitesse : document.getElementById("vitesse").value  
    }
    vitesse = document.getElementById("vitesse").value
    socket.send(JSON.stringify(json_config));
    send({ "vitesse": parseInt((document.getElementById("vitesse").value /MAX_VITESSE)*100) });
    compteurDisplayChange(JSON.stringify(json_config))
}

// change compteur display
const compteurDisplayChange = (event) => {

    let config = JSON.parse(event);
    console.log(config.vitesse)
    document.getElementById("vitesse").value = config.vitesse;
    document.getElementById("compteur").innerHTML = parseInt((config.vitesse/MAX_VITESSE)*100) + "%";



}

document.querySelector("#video img").src = location.origin + "/stream.mjpg"


/**
 * 
 *  Init -> Fetch all config 
 * 

const init = () => {
    fetch(location.href, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then( (res) => {
        changeSpeed(res);
    }
    )};

 * */ 

function send(param) {
    fetch(location.href, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(param)
    }).then(res => {
        console.log(res);
    });
}

document.onkeydown = function (e) {
    e = e || window.event;
    touche = e.keyCode;
    if (touche != touche1) {
        touche1 = touche
        if (touche == "38" || touche == "90") {
            sens = 0;
        } else if (touche == "39" || touche == "68") {
            sens = 1;
        } else if (touche == "40" || touche == "83") {
            sens = 2;
        } else if (touche == "37" || touche == "81") {
            sens = 3;
        }
        send({ "sens": sens });
    }
};
document.onkeyup = function () {
    if (touche == "37" || touche == "38" || touche == "39" || touche == "40" || touche == "90" || touche == "81" || touche == "83" || touche == "68") {
        sens = 4;
        send({ "sens": sens });
        touche1 = "";
    }
}

document.getElementById("pantiltvideo").onmousedown = function (e) {
    let canv = this.getBoundingClientRect();
    x = e.clientX - canv.left;
    y = e.clientY - canv.top;
    pan = Math.round((1 - (x / canv.width)) * 450 + 150);
    tilt = Math.round((y / canv.height) * 400 + 200);
    send({ "pan": pan, "tilt": tilt });
}

//Contrôles visuels

document.getElementById("reset").onclick = function () {
    pan = 375;
    tilt = 400;
    send({ "pan": pan, "tilt": tilt });
}

document.getElementById("eteindrePi").onclick = function () {
    send({ "pi": 0 });
    alert('Robot éteint');
}

document.getElementById("avancer").onclick = function () {
    sens = 0;
    send({ "sens": sens });
}

document.getElementById("droite").onclick = function () {
    sens = 1;
    send({ "sens": sens });
}

document.getElementById("reculer").onclick = function () {
    sens = 2;
    send({ "sens": sens });
}

document.getElementById("gauche").onclick = function () {
    sens = 3;
    send({ "sens": sens });
}

document.getElementById("stop").onclick = function () {
    sens = 4;
    send({ "sens": sens });
}
